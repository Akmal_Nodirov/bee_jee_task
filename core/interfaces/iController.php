<?php
namespace app\core\interfaces;

interface iController{
	protected function beforeAction();
	protected function afterAction();
	protected function beforeAction($route);
}
?>