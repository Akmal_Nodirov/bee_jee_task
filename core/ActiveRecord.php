<?php
namespace app\core;

use app\core\helpers\DB;

class ActiveRecord extends BaseModel{
	
	protected static $db = null;

	protected $table = null;
	protected $db_name = null;

	public static function findBySql($sql, $args = [], $type = "view"){
		return static::DB()->query($sql, $args, $type);
	}	

	public static function DB(){
		if(!static::$db){
			static::$db = new DB();
		}
		return static::$db;
	}

	public static function save($sql, $args = [], $type = "add"){
		return static::DB()->query($sql, $args, $type);
	}

	public static function update($sql, $args = [], $type = "add"){
		return static::DB()->query($sql, $args, $type);
	}

	public static function login($username, $password, $args = [], $type='view'){

		$sql = 'select * from express_user where username= "'.$username.'" and password = '.$password;

		return static::DB()->query($sql, $args, $type);

	}	
}
?>