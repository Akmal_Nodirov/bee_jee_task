<?php
namespace app\core;

class Request{

	public $params = null;

	public function get($name = null){
		$get = $this->getDefault();
		if($name)
			return isset($get[$name]) ? $get[$name] : [];
		return $get;
	}

	public function getDefault(){
		if(empty($this->params))
			return [];
		$params = explode("&", $this->params);
		$array = [];

		foreach($params as $param){
			$arr = explode("=", $param);
			if(empty($arr ))
				continue;
			$array[$arr[0]] = isset($arr[1]) ? $arr[1] : null;
		}

		return $array;
	}
}
