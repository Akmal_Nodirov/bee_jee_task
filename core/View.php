<?php
namespace app\core;

class View{

	use traits\Magic;
	
	public $viewPath ;
	

	public function render($path, $var = []){
        ob_start();
        if(!empty($var))
        	extract($var);
        require "{$this->viewPath}/{$path}.php";
		return ob_get_clean();
	}
}

?>