<?php
namespace app\core\helpers;

class DB{


    public $db;
    public function __construct($db = "db_one")
    {

        $db_config = include __DIR__ . "/../../config/db.php";
        
        $this->db = new \PDO("mysql:dbname=".$db_config[$db]['db_name'].";host".$db_config[$db]['db_host'], $db_config[$db]['db_username'], $db_config[$db]['db_password']);
        
        $this->db->query("SET NAMES 'utf8' ");
    }

    public function query($sql, $args = [],  $type = "view"){
        $stmt = $this->execute($sql, $args);
        if($type == "add"){

            return $this->db->lastInsertId();
        }elseif($type == "view"){
            
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);   
            
        }
    }

    public function execute($sql, $args = array()){
            $stmt = $this->db->prepare($sql);
        
        try {
            $stmt->execute($args);
            
        } catch (\PDOException $e) {
            $this->db->errorInfo();
        }   
        return $stmt;
    }
}
?>