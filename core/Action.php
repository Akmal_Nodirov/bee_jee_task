<?php

namespace app\core;

use app\core\interfaces\iController;
use app\core\Router;

class Action
{
	protected static $prefix = "Controller";

	protected static $controllerPath = "app/controllers";

	protected static $layoutPath = "app/views";

	protected static $defaultNamespace = "app\controllers";

	public static $action_name = '';


	protected function routeConfig(Router $router)
	{
		$router->add('', ['controller' => 'Home', 'action' => 'index']);

		$router->add('posts', ['controller' => 'Posts', 'action' => 'index']);

		$router->add('admin/{action}/{controller}&*{{params}}', []);

		$router->add('{controller}/{action}&*{{params}}');
		return $router;
	}

	public function getRoutes($url = ""){
		$router = $this->routeConfig(new Router());
		$result = $router->match($url);
		
		if(!$result)
			return false;

		return $router->getParams();

	}

	public function run($url = ""){

		session_start();

		$params = static::getRoutes($url);

		if(!($controller_name = $params['controller']))
			return false;
		if(!($action = $params['action']))
			return;

		static::$action_name = $params['action'];

		$class = static::$defaultNamespace."\\".ucfirst($controller_name).static::$prefix;
		if(!class_exists($class)){
			header("HTTP/1.1 404 Not Found");
			exit;
		}
		
		$object = new $class;

		if(isset($params['params'])){
			$object::$urlRequest = $params['params'];
		}
		return $object->action($controller_name, $action);
	}



}