<?php

namespace app\core;

use app\core\interfaces\iController;
use app\core\Router;
use app\core\View;

class BaseController
{
	use  \app\core\traits\Magic;

	protected static $action = "action";
	protected static $viewModel = null;
	protected static $controller_name = null;
	protected static $requestModel = null;
	public static $urlRequest = [];

	protected $layoutPath = "layouts";
	protected $layoutName = "main";
	protected $viewPath = "../views/";
	protected $title = null;

	protected function beforeAction($action){

	}

	protected function afterAction(){

	}

	public function action($controller_name, $action){
		static::$controller_name = strtolower($controller_name);

		$result = $this->getAction($action);

		return $result;
	}

	protected function getAction($action){
		$func = static::$action.ucfirst($action);

		return $this->$func();
	}

	public function render($index, $params = []){

		$path = static::$controller_name."/".$index;

		$content = $this->renderDefault($path, $params);

		$layoutPath = $this->layoutPath. "/" . $this->layoutName;

		return $this->renderDefault($layoutPath, [
			"content" => $content, 
			"title" => $this->title
		]);
	}

	protected function renderDefault($path, $params = []){

		$view = static::View();
		$view->viewPath = $this->viewPath;
		return $view->render($path, $params);
	}

	protected static function View(){
		if(!static::$viewModel)
			static::$viewModel= new View;
		return static::$viewModel;
	}

	protected static function Request(){

		if(!static::$requestModel){
			static::$requestModel =  new Request;
			static::$requestModel->params = static::$urlRequest;
		}

		return static::$requestModel;
	}

	protected function redirect($page, $params = []){
		$url = $page;
		$i = 0;

		foreach($params as $key => $value){

			if($i != 0)
				$queryString .= "&";
			$queryString .= $key."=".$value;
			$i++;
		}

		$url .= "?".$queryString;
		header("Location: ".$url);
		exit;
	}
}