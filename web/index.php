<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require "../vendor/autoload.php";

$url = $_SERVER['QUERY_STRING'];


$action = new app\core\Action();

echo $action->run($url);
