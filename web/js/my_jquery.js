	

	$(document).on('ready', function(){

		$(document).on('click', '.preview_button', function(){
			
			var values = [];
			var html = '';

			$('.form-group input').each(function(index, value){
				values[$(value).attr('id')] = $(value).val();
			})

			values['task_text'] = $('.bee_jee_textarea').val();

			  	html = '<table class="table">'+
			    	'<thead>'+
			      		'<tr>'+
			        	'<th>Username</th>'+
			        	'<th>Email</th>'+
			        	'<th>Text</th>'+
			        	'<th>Image</th>'+
			        	'<th>Status</th>'+
			      '</tr>'+
			    '</thead>'+
			   	'<tbody>'+
			      '<tr>'+
			        '<td>'+values['username']+'</td>'+
			       	'<td>'+values['email']+'</td>'+
			        '<td>'+values['task_text']+'</td>'+
			        '<td>'+values['task']+'</td>'+
			        '<td>'+values['status']+'</td>'+
			      '</tr>'+
			    '</tbody>'+
			  	'</table>';

			  	$('.bee_jee_preview_modal').html(html);
			  	$('#exampleModalCenter').modal('toggle');

		})
		
		
	})


	$(document).on('click', '.bee_jee_task_status', function(){

		var task_id = $(this).attr('model_id');
		var status = '';

		if($(this).prop('checked') == true){
			status = 1;
		} else {
			status = 0;
		}

		  $.ajax({
            url: '/home/taskstatus',
            data: {task_id: task_id, status: status},
            success: function(response){
            	window.location.reload();
            	if(response){
            		console.log('saved');
            	} else {
            		console.log('not saved');
            	}
            }
        });

	})
