<?php

namespace app\controllers;

use app\core\BaseController;
use app\core\ActiveRecord;
use app\core\Request;

class HomeController extends BaseController
{

	public function actionIndex(){
		
		$page_start = 0;
		$page = 1;
		$type = 'asc';
		$order_by = 'username';

		if(isset($_GET['page'])){
			$page = $_GET['page'];
			$page_start = $page * 3 - 3;

		}

		if(isset($_GET['type'])){
			$type = $_GET['type']=='asc' ? 'desc' : 'asc';
		}

		if(isset($_GET['order_by'])){
			$order_by = $_GET['order_by'];
		}

		$pagination_count = ceil(count(ActiveRecord::findBySql('select * from express_tasks'))/3);

		$tasks = ActiveRecord::findBySql('select * from express_tasks order by '.$order_by.' '.$type.' limit '.$page_start.',3');

		return $this->render("index", [
			'tasks' => $tasks,
			'pagination_count' => $pagination_count,
			'page' => $page,
			'type' => $type,
		]) ;

	}

	public function actionEnteringdata(){

		$errors = [];

		if(isset($_POST['submit_button'])){

			

			$username = $_POST['username'];
			$email = $_POST['email'];
			$task = $_POST['text_task'];
			$status = $_POST['status'];
			$image = $_FILES['image_file'];

			if(!$status){
				$status = 0;
			}

		
			foreach($_POST as $key => $value){

		    	if($key != 'status' && $key != 'submit_button' &&  empty($value)){
		    		$errors[] = $key.' field cannot be empty';
		    	}
		    }


			if(isset($image) && empty($image['error']) && count($errors) <=0){

				$uploaddir = 'uploads/';
     			$uploadfile = $uploaddir . basename($_FILES['image_file']['name']);

				if($this->imageUploading($_FILES) != 'true'){
					$errors[] = $this->imageUploading($_FILES);
				}

		    }


		    if(count($errors) <=0){

				ActiveRecord::save("INSERT INTO express_tasks (username, email, text, image, status)
				VALUES ('".$username."','".$email."','".$task."','".$uploadfile."','".$status."')");
				return $this->redirect('/home/index');
		    }

		}

		return $this->render("enter_data", [
			'errors' => $errors,
		]) ;

	}

	public function actionLogin(){

		if(isset($_POST['login'])){

			if(isset($_POST['username']) && isset($_POST['password'])){

				$username = $_POST['username'];
				$password = $_POST['password'];

				$data = ActiveRecord::login($username, $password);

				if(count($data) <= 0){

					$_SESSION['fail'] = 'Uncorrect name or password!'; 

				} else {

					$_SESSION['logged'] = 'Welcome '.$username;
      				$_SESSION['username'] = $username;
      				unset($_SESSION['fail']);
      				return $this->redirect('/home/index');
				}

			}

		}
		
		return $this->render("login") ;

	}

	public function actionLogout(){

		unset($_SESSION['logged']);
		unset($_SESSION['username']);

		return $this->redirect('/home/index') ;

	}


	public function imageUploading($image){

			$data = true;

			$image_types = ['jpeg', 'gif', 'png'];
			$file_type = explode('/', $_FILES['image_file']['type'])[1];

			if (!in_array($file_type, $image_types)){

				$data = 'Images type mustbe gif, jpg, or png';
				return $data;

			}


			$uploaddir = 'uploads/';

	      	if(!is_writable($uploaddir)){
	      		echo 'folder is not writable !'; die();
	      	} 

     		$uploadfile = $uploaddir . basename($_FILES['image_file']['name']);
      		$data = move_uploaded_file($_FILES['image_file']['tmp_name'], $uploadfile);

			$th_max_width = 320;
			$th_max_height = 240;
			

	    	list($width, $height, $type, $mime) = getimagesize($uploadfile);
	  		$ratio = ($width > $height) ? $th_max_width/$width : $th_max_height/$height; 
			$newwidth = round($width * $ratio); 
			$newheight = round($height * $ratio);

			$resized_image = imagecreatetruecolor($newwidth, $newheight);

			switch ($file_type){
				case 'gif': 
				  $source = imagecreatefromgif($uploadfile); 
				  break; 

				case 'jpeg': 
				  $source = imagecreatefromjpeg($uploadfile); 
				  break; 
				
				case 'png': 
				  $source = imagecreatefrompng($uploadfile); 
				  break;  
			}

			$data = imagecopyresampled($resized_image, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

			if($data){
				$data = 'true';
			} else {
				$data = 'false';
			}

			return $data;
	}

	public function actionTaskstatus(){

		$data = false;

		if(isset($_GET['task_id']) && isset($_GET['status'])){

			$task_id = $_GET['task_id'];
			$status = $_GET['status'];
			$sql = 'UPDATE express_tasks SET status = '.$status.' where id = '.$task_id;
			ActiveRecord::update($sql);
			return $this->redirect('/home/index');
			$data = true;

		}

		return $data;

	}

}