

<?php

use app\core\Action;


	$status = '';
	$user = '';
	if(isset($_SESSION['logged'])){
      	$status = 'logged';
      	$user = $_SESSION['username'];
    } else {
     	$status = 'guest';
    }

    $action = Action::$action_name;

?>


<?php require "../config/main_config.php";?>





<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name = "viewport" content ="width=device-width, initial-scale=1">
	<title>express_task</title>
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
</head>
<body>
	<br>
	<div class="container">
		<ul class="nav nav-pills nav-justified">
			<li class='<?php if($action=='index'){echo 'active';}?>'><a href="<?=$main_config['base']['url'].'/home/index'?>">Home</a></li>
			<li class='<?php if($action=='enteringdata'){echo 'active';}?>'><a href="<?=$main_config['base']['url'].'/home/enteringdata'?>">Entering Data</a></li>

			<?php if($status=='guest'):?>
				<li class='<?php if($action=='login'){echo 'active';}?>'><a href="<?=$main_config['base']['url'].'/home/login'?>">Login</a></li>
			<?php endif;?>

			<?php if($status=='logged'):?>
				<li><a href="<?=$main_config['base']['url'].'/home/logout'?>">Logout(<?=$user?>)</a></li>
			<?php endif;?>

		</ul>

	<?php echo $content;?>

	</div>
	<script type="text/javascript" src="/js/jQuery.js"></script>
	<script type="text/javascript" src="/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/js/my_jquery.js"></script>

</body>


</html>