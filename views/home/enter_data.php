

<h2>Creating a new task</h2>

<div class="col-md-7">
	

	<?php foreach($errors as $items):?>
		<?php echo $items.'<br />'?>
	<?php endforeach?>


</div>



<div class="col-md-7">
<form class="form-horizontal my_bee_jee_form" action="/home/enteringdata" method="POST" enctype="multipart/form-data">

  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="username" placeholder="Email" name="username">
    </div>
  </div>

  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="email" placeholder="email" name="email">
    </div>
  </div>

   <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Text task</label>
    <div class="col-sm-10">
      <textarea type="text" class="form-control bee_jee_textarea" id="task" placeholder="task" rows="6" name="text_task"></textarea>
    </div>
  </div>

   <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Image</label>
    <div class="col-sm-10">
      <input type="file" class="form-control" id="task" placeholder="task" name="image_file">
    </div>
  </div>

    <?php $checked = '';?>
    <div class="form-group">
	    <label for="inputPassword3" class="col-sm-2 control-label">Task status</label>
	    <div class="col-sm-10">
	      <input type="checkbox" class="status" id="status" value=1 name="status" <?=$checked?> >
	    </div>
	</div>
  <div class="form-group">

    <div class="col-sm-offset-2 col-sm-10">
      	<button type="submit" class="btn btn-default" name="submit_button">Enter</button>
      	<span class="btn btn-default preview_button" name="">Preview</span>
    </div>

  </div>

</form>
</div>


<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Task List</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="bee_jee_preview_modal"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



