
  <h2>Task list</h2>

  <?php $disabled = 'disabled';?>
  <?php if(isset($_SESSION['logged'])){$disabled = '';}?>

         
  <table class="table">
    <thead>
      <tr>
        <th>user id</th>
        <th><a href = "/home/index?order_by = username&type=<?=$type?>&page=<?=$page?>">Username</a></th>
        <th><a href = "/home/index?order_by = email&type=<?=$type?>&page=<?=$page?>">Email</a></th>
        <th>Text</th>
        <th>Image</th>
        <th><a href = "/home/index?order_by = status&type=<?=$type?>&page=<?=$page?>">Status</a></th>
        <th>Check</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach($tasks as $items):?>
      <tr>
        <td><?=$items['id']?></td>
        <td><?=$items['username']?></td>
        <td><?=$items['email']?></td>
        <td><?=$items['text']?></td>
        <td><img src="/<?=$items['image']?>" width="100"></td>
        <td><?php if($items['status'] == 1){echo 'Done'; $status = "checked";} else {echo 'Undone'; $status="";}?></td>
        <td> <input type="checkbox" class="bee_jee_task_status" id="status" value=1 name="status" model_id = "<?=$items['id']?>" data-status="<?=$items['status']?>" <?=$disabled?> <?=$status?>></td>
      </tr>
  	<?php endforeach;?>
    </tbody>
  </table>
</div>

<div style="display: flex; justify-content: center;">
<nav aria-label="Page navigation">
  <ul class="pagination">

    <?php $active = '';?>
    <?php $prev = $page-1; if(($page-1) <= 1){$prev = 1;}?>

    <li>
      <a href="/home/index?page=<?=$prev?>" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>

    <?php for($i = 1; $i <= $pagination_count; $i++):?>
    	<li class="<?php if($i==$page){echo 'active';}?>"><a href="/home/index?page=<?=$i?>"><?=$i?></a></li>
    <?php endfor;?>

    <?php $next = $page+1; if(($page+1) >= $i){$next = $i-1;}?>

    <li>
      <a href="/home/index?page=<?=$next?>" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>

  </ul>
</nav>
</div>